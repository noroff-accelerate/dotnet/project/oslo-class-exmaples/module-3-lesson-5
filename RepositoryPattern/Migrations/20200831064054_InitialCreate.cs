﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RepositoryPattern.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Professor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Position = table.Column<string>(nullable: true),
                    Dob = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Professor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Student",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Course = table.Column<string>(nullable: true),
                    Dob = table.Column<DateTime>(nullable: false),
                    ProfessorId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Student", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Student_Professor_ProfessorId",
                        column: x => x.ProfessorId,
                        principalTable: "Professor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Professor",
                columns: new[] { "Id", "Dob", "Name", "Position" },
                values: new object[] { 1, new DateTime(2020, 8, 30, 8, 40, 53, 649, DateTimeKind.Local).AddTicks(2194), "Nicholas Lennox", "Scuffed Lecturer" });

            migrationBuilder.InsertData(
                table: "Professor",
                columns: new[] { "Id", "Dob", "Name", "Position" },
                values: new object[] { 2, new DateTime(1993, 4, 15, 8, 40, 53, 653, DateTimeKind.Local).AddTicks(3772), "Dewald Els", "Front end chap" });

            migrationBuilder.InsertData(
                table: "Professor",
                columns: new[] { "Id", "Dob", "Name", "Position" },
                values: new object[] { 3, new DateTime(1993, 4, 14, 8, 40, 53, 653, DateTimeKind.Local).AddTicks(3918), "Craig Marias", "Stuck in Sweden" });

            migrationBuilder.InsertData(
                table: "Student",
                columns: new[] { "Id", "Course", "Dob", "Name", "ProfessorId" },
                values: new object[] { 1, "Red pill vs blue pill", new DateTime(1999, 8, 31, 8, 40, 53, 653, DateTimeKind.Local).AddTicks(6559), "Mr Anderson", 1 });

            migrationBuilder.InsertData(
                table: "Student",
                columns: new[] { "Id", "Course", "Dob", "Name", "ProfessorId" },
                values: new object[] { 2, "Being the boss and other joys", new DateTime(1979, 8, 31, 8, 40, 53, 653, DateTimeKind.Local).AddTicks(7369), "Erlend", 3 });

            migrationBuilder.CreateIndex(
                name: "IX_Student_ProfessorId",
                table: "Student",
                column: "ProfessorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Student");

            migrationBuilder.DropTable(
                name: "Professor");
        }
    }
}
