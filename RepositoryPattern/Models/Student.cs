﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryPattern.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Course { get; set; }
        public DateTime Dob { get; set; }

        public int ProfessorId { get; set; }

        public Professor Professor { get; set; }
    }
}
