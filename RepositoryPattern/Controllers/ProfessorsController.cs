﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RepositoryPattern.Data;
using RepositoryPattern.Models;

namespace RepositoryPattern.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfessorsController : ControllerBase
    {
        private readonly SchoolDbContext _context;

        public ProfessorsController(SchoolDbContext context)
        {
            _context = context;
        }

        // GET: api/Professors
        [HttpGet]
        public ActionResult<IEnumerable<Professor>> GetProfessor()
        {
            return _context.Professor.ToList();
        }

        // GET: api/Professors/5
        [HttpGet("{id}")]
        public ActionResult<Professor> GetProfessor(int id)
        {
            var professor = _context.Professor.Find(id);

            if (professor == null)
            {
                return NotFound();
            }

            return professor;
        }

        // PUT: api/Professors/5
        [HttpPut("{id}")]
        public IActionResult PutProfessor(int id, Professor professor)
        {
            if (id != professor.Id)
            {
                return BadRequest();
            }

            _context.Entry(professor).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProfessorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Professors
        [HttpPost]
        public ActionResult<Professor> PostProfessor(Professor professor)
        {
            _context.Professor.Add(professor);
            _context.SaveChanges();

            return CreatedAtAction("GetProfessor", new { id = professor.Id }, professor);
        }

        // DELETE: api/Professors/5
        [HttpDelete("{id}")]
        public ActionResult<Professor> DeleteProfessor(int id)
        {
            var professor = _context.Professor.Find(id);
            if (professor == null)
            {
                return NotFound();
            }

            _context.Professor.Remove(professor);
            _context.SaveChanges();

            return professor;
        }

        private bool ProfessorExists(int id)
        {
            return _context.Professor.Any(e => e.Id == id);
        }
    }
}
