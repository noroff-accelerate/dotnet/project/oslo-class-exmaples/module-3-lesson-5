﻿using Microsoft.EntityFrameworkCore;
using RepositoryPattern.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryPattern.Data
{
    public class SchoolDbContext : DbContext
    {
        public DbSet<Professor> Professor { get; set; }
        public DbSet<Student> Student { get; set; }

        public SchoolDbContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed some data
            modelBuilder.Entity<Professor>().HasData(new Professor() { Id=1, Name="Nicholas Lennox", Position="Scuffed Lecturer", Dob=DateTime.Now.AddDays(-1)});
            modelBuilder.Entity<Professor>().HasData(new Professor() { Id=2, Name="Dewald Els", Position="Front end chap", Dob=DateTime.Now.AddDays(-10000)});
            modelBuilder.Entity<Professor>().HasData(new Professor() { Id=3, Name="Craig Marias", Position="Stuck in Sweden", Dob=DateTime.Now.AddDays(-10001)});

            modelBuilder.Entity<Student>().HasData(new Student() { Id=1, ProfessorId=1, Name="Mr Anderson", Course="Red pill vs blue pill", Dob=DateTime.Now.AddYears(-21)});
            modelBuilder.Entity<Student>().HasData(new Student() { Id=2, ProfessorId=3, Name="Erlend", Course="Being the boss and other joys", Dob=DateTime.Now.AddYears(-41)});
        }
    }
}
